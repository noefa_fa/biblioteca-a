from django.db import models
from django.contrib.auth.models import AbstractUser
#from djtriggers.models import Trigger
from tabnanny import verbose
from requests import request


class Usuario(AbstractUser):
    # atributos adicionales.
    email = models.EmailField('email address', unique = True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']

    def __str__(self):
        return self.first_name + ' ' + self.last_name

"""
class TriggerEjemplar(Trigger):
    class Meta:
        # We need a regular model as the trigger specific data needs a
        # place to live in the db.
        proxy = False
"""

# Create your models here.
class Autor(models.Model):
    nombre = models.CharField(max_length=200, blank=True)
    apellido = models.CharField(max_length=200, blank=True)
    pseudonimo = models.CharField(max_length=200, blank=True)
    def __str__(self):
        return self.nombre + ' ' + self.apellido + ' ' + self.pseudonimo

class GeneroLiterario(models.Model):
    nombre = models.CharField(max_length=200)
    def __str__(self):
        return self.nombre

class Editorial(models.Model):
    nombre = models.CharField(max_length=200)
    def __str__(self):
        return self.nombre

class Libro(models.Model):
    titulo = models.CharField(max_length=200)
    autores = models.ManyToManyField(Autor)
    genero = models.ForeignKey(to=GeneroLiterario, on_delete=models.RESTRICT, default=1)
    editorial = models.ForeignKey(to=Editorial, on_delete=models.RESTRICT, default=1)
    imagen = models.ImageField(upload_to='imagenes/', verbose_name="Imagen", null=True)

    #autors = ''
    #print(autores.objects.get())
    def __str__(self):
        return self.titulo + ' ' + ' ' #+ self.editorial.nombre + ' ' + self.genero.nombre

class Ejemplar(models.Model):
    #disponible = models.BooleanField(verbose_name="Disponible")
    libro = models.ForeignKey(to=Libro, on_delete=models.RESTRICT, default=1)
    def __str__(self):
        return self.libro.titulo


class Prestamo(models.Model):
    Fecha_devolucion = models.DateField(null=True, blank=True)
    Fecha_entrega = models.DateField(null=True, blank=True)
    Limite_entrega = models.DateField()
    Limite_devolucion = models.DateField(null=True, blank=True)
    ejemplar = models.ForeignKey(to=Ejemplar, on_delete=models.RESTRICT)
    lector = models.ForeignKey(to=Usuario, on_delete=models.RESTRICT, related_name="lector")
    bibliotecario_entrega = models.ForeignKey(to=Usuario, on_delete=models.SET_NULL, null=True, blank=True, related_name="entrega")
    bibliotecario_recibe = models.ForeignKey(to=Usuario, on_delete=models.SET_NULL, null=True, blank=True, related_name="recibe")
    def __str__(self):
        return 'Préstamo ID: ' + str(self.id) + ', ' + str(self.ejemplar.libro.titulo)
