from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.template import loader
from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.contrib import messages
from django.db.models import Count
from django.db import models
import json
from members.prestamo import Prestamo

from .models import Usuario, Libro, Autor, GeneroLiterario, Editorial, Ejemplar
from .forms import LibroForm, AutorForm, GeneroLiterarioForm, EditorialForm


import csv


# Decorador para verificar grupo.
def es_bibliotecario(user):
    # usuario autenticado?
    if (user.is_authenticated):
        # usuario pertenece al grupo Bibliotecario?
        return user.groups.filter(name='Bibliotecario').exists()
    else:
        return False

# Libros

# Create your views here.
@user_passes_test(es_bibliotecario)
def index(request):
    template = loader.get_template('bibliotecario/libros/index.html')

    libros = Libro.objects.all()
    print(libros[0])
    ejemplares = Ejemplar.objects.filter(libro=libros).values()
    #print(ejemplares[0]["disponible"])
    #print(ejemplares[0]["disponible"])
    #ejemplares = Ejemplar.objects.filter(libro=libros)

    context = {
        'libros': libros,
        'ejemplares': ejemplares,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))

@user_passes_test(es_bibliotecario)
def index_autor(request):
    template = loader.get_template('bibliotecario/autor/index.html')

    autores = Autor.objects.all()
    #ejemplares = Ejemplar.objects.filter(libro=libros)

    context = {
        'autores': autores,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))


@user_passes_test(es_bibliotecario)
def add(request):
    if (request.method == 'GET'):
        form = LibroForm()
        # muesta usarios de esos grupos.
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        #queryset = Usuario.objects.filter(groups__name__in=['bibliotecario', 'Director'])

        # mostrar formulario.
        template = loader.get_template('bibliotecario/libros/add.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = LibroForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('index')

@user_passes_test(es_bibliotecario)
def add_autor(request):
    if (request.method == 'GET'):
        form = AutorForm()
        # muesta usarios de esos grupos.
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        #queryset = Usuario.objects.filter(groups__name__in=['bibliotecario', 'Director'])

        # mostrar formulario.
        template = loader.get_template('bibliotecario/autor/add.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = AutorForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('index_autor')



@user_passes_test(es_bibliotecario)
def delete(request, id):
    libro = Libro.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario.
        template = loader.get_template('bibliotecario/libros/delete.html')

        context = {
            'settings': settings,
            'libro': libro,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # elimina la publicación.
        libro.delete()
        messages.success(request, "Eliminación realizada.")

        return redirect('index')

@user_passes_test(es_bibliotecario)
def delete_autor(request, id):
    autor = Autor.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario.
        template = loader.get_template('bibliotecario/autor/delete.html')

        context = {
            'settings': settings,
            'autor': autor,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # elimina la publicación.
        autor.delete()
        messages.success(request, "Eliminación realizada.")

        return redirect('index_autor')

@user_passes_test(es_bibliotecario)
def update(request, id):
    libro = Libro.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario con datos.
        form = LibroForm(instance=libro)
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        template = loader.get_template('bibliotecario/libros/add.html')

        context = {
            'libro': libro,
            'settings': settings,
            'form': form,
            'isadd': False,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # actualiza datos.
        form = LibroForm(request.POST, instance=libro)
        if form.is_valid():
            form.save()
            messages.success(request, "Actualización realizada.")

        return redirect('index')

@user_passes_test(es_bibliotecario)
def update_autor(request, id):
    autor = Autor.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario con datos.
        form = AutorForm(instance=autor)
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        template = loader.get_template('bibliotecario/autor/add.html')

        context = {
            'autor': autor,
            'settings': settings,
            'form': form,
            'isadd': False,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # actualiza datos.
        form = AutorForm(request.POST, instance=autor)
        if form.is_valid():
            form.save()
            messages.success(request, "Actualización realizada.")

        return redirect('index_autor')

def register_request(request):
	if request.method == "POST":
		form = NewUserForm(request.POST)
		if form.is_valid():
			user = form.save()
			login(request, user)
			messages.success(request, "Registration successful." )
			return redirect("main:homepage")
		messages.error(request, "Unsuccessful registration. Invalid information.")
	form = NewUserForm()
	return render (request=request, template_name="main/register.html", context={"register_form":form})

# Lector

# Decorador para verificar grupo.
def es_lector(user):
    # usuario autenticado?
    if (user.is_authenticated):
        # usuario pertenece al grupo lector?
        return user.groups.filter(name='Lector').exists()
    else:
        return False


# Create your views here.
@user_passes_test(es_lector)
def index_lector(request):
    template = loader.get_template('lector/catalogo.html')

    libros = Libro.objects.all()
    #ejemplares = Ejemplar.objects.filter(libro=libros)

    context = {
        'libros': libros,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))

## Generos

## Generos

# Create your views here.
@user_passes_test(es_bibliotecario)
def index_genero(request):
    template = loader.get_template('bibliotecario/genero/index.html')

    generos = GeneroLiterario.objects.all()

    context = {
        'generos': generos,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))


@user_passes_test(es_bibliotecario)
def add_genero(request):
    if (request.method == 'GET'):
        form = GeneroLiterarioForm()
        # muesta usarios de esos grupos.
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        #queryset = Usuario.objects.filter(groups__name__in=['bibliotecario', 'Director'])

        # mostrar formulario.
        template = loader.get_template('bibliotecario/genero/add.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = GeneroLiterarioForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('index_genero')

@user_passes_test(es_bibliotecario)
def delete_genero(request, id):
    generos = GeneroLiterario.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario.
        template = loader.get_template('bibliotecario/genero/delete.html')

        context = {
            'generos': generos,
            'settings': settings,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # elimina la publicación.
        generos.delete()
        messages.success(request, "Eliminación realizada.")

        return redirect('index_genero')

@user_passes_test(es_bibliotecario)
def update_genero(request, id):
    generos = GeneroLiterario.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario con datos.
        form = GeneroLiterarioForm(instance=generos)
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        template = loader.get_template('bibliotecario/genero/add.html')

        context = {
            'generos': generos,
            'settings': settings,
            'form': form,
            'isadd': False,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # actualiza datos.
        form = GeneroLiterarioForm(request.POST, instance=generos)
        if form.is_valid():
            form.save()
            messages.success(request, "Actualización realizada.")

        return redirect('index_genero')

# Editorial

# Create your views here.
@user_passes_test(es_bibliotecario)
def index_editorial(request):
    template = loader.get_template('bibliotecario/editorial/index.html')

    editorial = Editorial.objects.all()

    context = {
        'editorial': editorial,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))


@user_passes_test(es_bibliotecario)
def add_editorial(request):
    if (request.method == 'GET'):
        form = EditorialForm()
        # muesta usarios de esos grupos.
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        #queryset = Usuario.objects.filter(groups__name__in=['bibliotecario', 'Director'])

        # mostrar formulario.
        template = loader.get_template('bibliotecario/editorial/add.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = EditorialForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('index_editorial')

@user_passes_test(es_bibliotecario)
def delete_editorial(request, id):
    editorial = Editorial.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario.
        template = loader.get_template('bibliotecario/editorial/delete.html')

        context = {
            'editorial': editorial,
            'settings': settings,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # elimina la publicación.
        generos.delete()
        messages.success(request, "Eliminación realizada.")

        return redirect('index_editorial')

@user_passes_test(es_bibliotecario)
def update_editorial(request, id):
    generos = Editorial.objects.get(id=id)

    if (request.method == 'GET'):
        # muestra el formulario con datos.
        form = EditorialForm(instance=generos)
        #form.fields['bibliotecario'].queryset = Usuario.objects.filter(groups__name__in=['Bibliotecario'])
        template = loader.get_template('bibliotecario/editorial/add.html')

        context = {
            'editorial': editorial,
            'settings': settings,
            'form': form,
            'isadd': False,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # actualiza datos.
        form = EditorialForm(request.POST, instance=generos)
        if form.is_valid():
            form.save()
            messages.success(request, "Actualización realizada.")

        return redirect('index_editorial')



# home page.
def grafico(request):
    # variable de session num_visitas.
    num_visitas = request.session.get('num_visitas', 0)
    request.session['num_visitas'] = num_visitas + 1

    # default template.
    template = loader.get_template('bibliotecario/grafico/dashboard.html')

    libros = Libro.objects.all()

    # agrupa número de publicaciones por el año.
    result = (libros.values('genero').annotate(dcount=Count('genero')).order_by('genero'))

    # copia a un diccionario el rsultado.
    dataarticulos = []

    #count = 0
    for item in result:
        genre = GeneroLiterario.objects.filter(id=item['genero']).values()[0]
        dataarticulos.append({'label': genre["nombre"], 'y': item["dcount"]})


    # gráfico ejemplares.
    ejemplares = Ejemplar.objects.all()


    result2 = (ejemplares.values('libro_id').annotate(dcount=Count('libro_id')))
    print(result2)


    dataejemplares = []
    for item in result2:
        obj = Libro.objects.filter(id=item['libro_id']).values()[0]
        dataejemplares.append({'label': obj["titulo"], 'y': item['dcount']})

    print(dataejemplares)

    context = {
        'settings': settings,
        'dataarticulos': json.dumps(dataarticulos),
        'dataejemplares': json.dumps(dataejemplares),
        'num_visitas': num_visitas,
    }
    #print(json.dumps(dataarticulos))

    #a_csv(dataarticulos, dataejemplares)
    return HttpResponse(template.render(context, request))

def agregar_articulo(request, id):
        prestamo = Prestamo(request)
        libro = Libro.objects.get(id=id)
        prestamo.agregar(libro)
        #return redirect('')
        return redirect('index_lector')

def eliminar_articulo(request, id):
        prestamo = Prestamo(request)
        libro = Libro.objects.get(id=id)
        prestamo.eliminar(libro)
        return redirect("index_lector")

def restar_articulo(request, id):
        prestamo = Prestamo(request)
        libro = Libro.objects.get(id=id)
        prestamo.restar(libro)
        return redirect("index_lector")

def limpiar_articulo(request):
        prestamo = Prestamo(request)
        prestamo.limpiar()
        return redirect("index_lector")
