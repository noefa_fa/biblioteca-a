from xml.dom.minidom import Document
from django.urls import path
from . import views

from members import prestamo

from django.conf import settings
from django.contrib.staticfiles.urls import static
urlpatterns = [
    path('libros/', views.index, name='index'),
    path('libros/add/', views.add, name='add'),
    path('libros/delete/<int:id>', views.delete, name='delete'),
    path('libros/update/<int:id>', views.update, name='update'),
    path('grafico/', views.grafico, name='grafico'),
    #path('grafico/csv/<queryset:a_csv>', views.a_csv, name="exportCSV"),

    path('autor/', views.index_autor, name='index_autor'),
    path('autor/add/', views.add_autor, name='add_autor'),
    path('autor/delete/<int:id>', views.delete_autor, name='delete_autor'),
    path('autor/update/<int:id>', views.update_autor, name='update_autor'),

    path('genero/', views.index_genero, name='index_genero'),
    path('genero/add/', views.add_genero, name='add_genero'),
    path('genero/delete/<int:id>', views.delete_genero, name='delete_genero'),
    path('genero/update/<int:id>', views.update_genero, name='update_genero'),

    path('editorial/', views.index_editorial, name='index_editorial'),
    path('editorial/add/', views.add_editorial, name='add_editorial'),
    path('editorial/delete/<int:id>', views.delete_editorial, name='delete_editorial'),
    path('editorial/update/<int:id>', views.update_editorial, name='update_editorial'),

    path('libros/agregar/<int:id>/', views.agregar_articulo, name="agregar"),
    path('eliminar/<int:id>/', views.eliminar_articulo, name="eliminar"),
    path('restar/<int:id>/', views.restar_articulo, name="restar"),
    path('limpiar', views.limpiar_articulo, name="limpiar"),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
