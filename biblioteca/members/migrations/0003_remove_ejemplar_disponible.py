# Generated by Django 4.0.5 on 2022-06-13 17:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0002_alter_ejemplar_disponible'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ejemplar',
            name='disponible',
        ),
    ]
