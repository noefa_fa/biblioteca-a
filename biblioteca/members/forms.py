from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import Usuario, Libro, Autor, GeneroLiterario, Editorial

###
class UsuarioCreationForm(UserCreationForm):
    class Meta:
        model = Usuario
        fields = ("username", "email")

class UsuarioChangeForm(UserChangeForm):
    class Meta:
        model = Usuario
        fields = ("username", "email")

class LibroForm(forms.ModelForm):
    class Meta:
        model = Libro
        fields = ("titulo", "autores", "genero", "editorial")

        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
            'autores': forms.SelectMultiple(attrs={'type': 'date', 'class': 'form-control', 'required': True}),
            'genero': forms.Select(attrs={'class': 'form-control', 'required': True}),
            'editorial': forms.Select(attrs={'class': 'form-control', 'required': True}),
        }

class AutorForm(forms.ModelForm):
    class Meta:
        model = Autor
        fields = ("nombre", "apellido", "pseudonimo")

        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
            'apellido': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
            'pseudonimo': forms.TextInput(attrs={'class': 'form-control', 'required': False}),
        }


class GeneroLiterarioForm(forms.ModelForm):
    class Meta:
        model = GeneroLiterario
        fields = ("nombre",)

        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
        }

class EditorialForm(forms.ModelForm):
    class Meta:
        model = Editorial
        fields = ("nombre",)

        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
        }
