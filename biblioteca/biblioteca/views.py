from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.template import loader
from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.contrib import messages
from django.urls import reverse
import json
from django.db.models import Count
from django.db.models.functions import TruncYear
from members.models import Libro, Autor, Editorial, GeneroLiterario


# home page.
def home(request):
    # variable de session num_visitas.
    num_visitas = request.session.get('num_visitas', 0)
    request.session['num_visitas'] = num_visitas + 1

    # default template.
    template = loader.get_template('home.html')
    context = {
        'settings': settings,
        'dataeditoriales': None,
        'num_visitas': num_visitas,
    }

    # está autenticado?
    if (request.user.is_authenticated):

        # es superusuario?
        if (request.user.is_superuser):
            return HttpResponseRedirect('admin/')

        # pertene a cierto grupo?
        if (len(request.user.groups.all()) > 0):
            # revisar el grupo (el primero).
            grupo = request.user.groups.all()[0]

            # cambia el template.
            if (grupo.name == "Lector"):
                template = loader.get_template('lector/catalogo.html')

                libros = Libro.objects.all()
                #ejemplares = Ejemplar.objects.filter(libro=libros)

                context = {
                'libros': libros,
                'settings': settings,
                }
                #return HttpResponse(template.render(context, request))
        else:
            # usuario sin grupo.
            pass
    else:
        # usuario no autenticado.
        pass

    return HttpResponse(template.render(context, request))
