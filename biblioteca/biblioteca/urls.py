from django.contrib import admin
from django.urls import include, path
from xml.dom.minidom import Document
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = [
    path('', views.home, name='home'),
    #path('bibliotecario/libros/', include('members.urls')),
    #path('bibliotecario/grafico/', include('members.urls')),
    path('bibliotecario/', include('members.urls')),
    #path('members/', include('members.urls')),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),


]
urlpatterns += staticfiles_urlpatterns()
